#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <math.h>

const float Link[4] = {0.078, 0.083, 0.066, 0.066};   //link_length[m]
float Kv_inv[4] = {10.0, 10.0, 10.0, 10.0};           //tauのゲインKv_inv
float Kv_[4] = {3.0, 3.0, 3.0, 1.0};                  //x,y,yaw,pitchのゲインKv_
float tau[4];
float theta[4];                                       //各軸の現在値
float input[4];                                       //joy_dataを格納する変数

//各軸の現在値を受け取る
void CurrentPos1(const dynamixel_msgs::JointState::ConstPtr& dynamixel1)
{
        theta[0] = dynamixel1->current_pos;
}
//2軸目は逆方向にモータが取り付けられているため、−をかける
void CurrentPos2(const dynamixel_msgs::JointState::ConstPtr& dynamixel2)
{
        theta[1] = -dynamixel2->current_pos;
}
void CurrentPos3(const dynamixel_msgs::JointState::ConstPtr& dynamixel3)
{
        theta[2] = dynamixel3->current_pos;
}
void CurrentPos4(const dynamixel_msgs::JointState::ConstPtr& dynamixel4)
{
        theta[3] = dynamixel4->current_pos;
}
//joy_dataを受け取る
//x:axes[1],y:axe[0](横),yaw:axes[4],pitch:axes[3]（横）
void joy_callback(const sensor_msgs::Joy::ConstPtr& joy) {
        input[0] = Kv_[0]*joy->axes[1];
        input[1] = Kv_[1]*joy->axes[0];
        input[2] = Kv_[2]*joy->axes[3];
        input[3] = Kv_[3]*joy->axes[4];
}
void Jacobian(float theta[], float input[], float tau[]) {
        //Jacobianの転置 Jb_t
        float Jb_t[3][3] = {
                {-(Link[0]*sin(theta[0]) + Link[1]*sin(theta[0] + theta[1]) + Link[2]*sin(theta[0] + theta[1] + theta[2])), Link[0]*cos(theta[0]) + Link[1]*cos(theta[0] + theta[1]) + Link[2]*cos(theta[0] + theta[1] + theta[2]), 1},
                {-(Link[1]*sin(theta[0] + theta[1]) + Link[2]*sin(theta[0] + theta[1] + theta[2])), Link[1]*cos(theta[0] + theta[1]) + Link[2]*cos(theta[0] + theta[1] + theta[2]), 1},
                {-Link[2]*sin(theta[0] + theta[1] + theta[2]),Link[2]*cos(theta[0] + theta[1] + theta[2]), 1},
        };
        tau[0] = Jb_t[0][0]*input[1] + Jb_t[0][1]*input[0] + Jb_t[0][2]*input[2];
        tau[1] = Jb_t[1][0]*input[1] + Jb_t[1][1]*input[0] + Jb_t[1][2]*input[2];
        tau[2] = Jb_t[2][0]*input[1] + Jb_t[2][1]*input[0] + Jb_t[2][2]*input[2];
        tau[3] = input[3];
}

int main(int argc, char** argv) {
        ros::init(argc, argv, "hand_controller");
        ros::NodeHandle n;

        // subscriiber
        ros::Subscriber joy_sub = n.subscribe("joy", 10, &joy_callback);
        ros::Subscriber cur_theta1_sub = n.subscribe("/tilt1_controller/state",10,&CurrentPos1);
        ros::Subscriber cur_theta2_sub = n.subscribe("/tilt2_controller/state",10,&CurrentPos2);
        ros::Subscriber cur_theta3_sub = n.subscribe("/tilt3_controller/state",10,&CurrentPos3);
        ros::Subscriber cur_theta4_sub = n.subscribe("/tilt4_controller/state",10,&CurrentPos4);

        //publisher
        ros::Publisher theta1_pub = n.advertise<std_msgs::Float64>("/tilt1_controller/command", 1);
        ros::Publisher theta2_pub = n.advertise<std_msgs::Float64>("/tilt2_controller/command", 1);
        ros::Publisher theta3_pub = n.advertise<std_msgs::Float64>("/tilt3_controller/command", 1);
        ros::Publisher theta4_pub = n.advertise<std_msgs::Float64>("/tilt4_controller/command", 1);

        //初期化
        std_msgs::Float64 command1,command2,command3,command4;
        float tau[4] = {0.0};
        float theta_dot[4] = {0.0};
        float delta_t = 0.02;
        int count = 0;

        ros::Rate loop_rate(50);
        while (ros::ok()) {
                Jacobian(theta, input, tau);
                for(int i=0; i<4; i++) {
                        theta_dot[i] = Kv_inv[i] * tau[i];
                }
                command1.data = theta[0] + theta_dot[0] * delta_t;
                command2.data = -(theta[1] + theta_dot[1] * delta_t);
                command3.data = theta[2] + theta_dot[2] * delta_t;
                command4.data = theta[3] + theta_dot[3] * delta_t;
                if(count <= 100) {
                        command1.data = 0.5;
                        command2.data = 1.0;
                        command3.data = 0.5;
                        command4.data = 1.0;
                        ROS_INFO("initialize finished");
                }
                count++;

                theta1_pub.publish(command1);
                theta2_pub.publish(command2);
                theta3_pub.publish(command3);
                theta4_pub.publish(command4);
                ros::spinOnce();
                loop_rate.sleep();
        }
}
