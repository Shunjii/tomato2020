#include <iostream>
#include <fstream>
#include <math.h>
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <dynamixel_msgs/JointState.h>
#include <sensor_msgs/Joy.h>

using namespace std;

///-- リンクの長さ
float Link[4] = {0.078, 0.083, 0.066, 0.066};
///-- dynamixelの角度変位に対するゲイン
float K_v_inv[5] = {5.0, 10.0, 5.0, 10.0, 10.0};
///-- グローバル変数定義
float input[5] = {0.0};
float theta[5] = {0.0};

///-- ジョイスティックの情報をsubscribeする関数
void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  ///-- 各自由度に対するゲイン
  input[0] = 2.0 * joy->axes[1]; ///- X
  input[1] = 2.0 * joy->axes[0]; ///- Y
  input[2] = 0.2 * joy->axes[2]; ///- Yaw
  input[3] = 1.0 * joy->axes[3]; ///- Pitch
  input[4] = 1.0 * (joy->buttons[0] - joy->buttons[1]); ///- End-Effector
}

///-- 以下5つ、dynamixelの現在角度をsubscribeする関数
void dynamixel1Callback(const dynamixel_msgs::JointState::ConstPtr& dynamixel1)
{
  theta[0] = dynamixel1->current_pos;
}

void dynamixel2Callback(const dynamixel_msgs::JointState::ConstPtr& dynamixel2)
{
  theta[1] = -dynamixel2->current_pos;
}

void dynamixel3Callback(const dynamixel_msgs::JointState::ConstPtr& dynamixel3)
{
  theta[2] = dynamixel3->current_pos;
}

void dynamixel4Callback(const dynamixel_msgs::JointState::ConstPtr& dynamixel4)
{
  theta[3] = dynamixel4->current_pos;
}

void dynamixel5Callback(const dynamixel_msgs::JointState::ConstPtr& dynamixel5)
{
  theta[4] = dynamixel5->current_pos;
}

///-- ヤコビ行列の転置を左からかける関数
void JacobCross(float theta[], float input[], float tau[])
{
  float Jacob_vT[3][3];

  Jacob_vT[0][0] = -(Link[0] * sin(theta[0]) + Link[1] * sin(theta[0]+theta[1]) + Link[2] * sin(theta[0]+theta[1]+theta[2]));
  Jacob_vT[0][1] =   Link[0] * cos(theta[0]) + Link[1] * cos(theta[0]+theta[1]) + Link[2] * cos(theta[0]+theta[1]+theta[2]);
  Jacob_vT[0][2] = 1;
  Jacob_vT[1][0] = -(Link[1] * sin(theta[0]+theta[1]) + Link[2] * sin(theta[0]+theta[1]+theta[2]));
  Jacob_vT[1][1] =   Link[1] * cos(theta[0]+theta[1]) + Link[2] * cos(theta[0]+theta[1]+theta[2]);
  Jacob_vT[1][2] = 1;
  Jacob_vT[2][0] = -Link[2] * sin(theta[0]+theta[1]+theta[2]);
  Jacob_vT[2][1] =  Link[2] * cos(theta[0]+theta[1]+theta[2]);
  Jacob_vT[2][2] = 1;

  for(int i=0; i<3; i++){
    tau[i] = Jacob_vT[i][0] * input[0] + Jacob_vT[i][1] * input[1] + Jacob_vT[i][2] * input[2];
  }

  tau[3] = input[3];
  tau[4] = input[4];
}

int main(int argc, char** argv)
{
  ///-- nodeの初期化、"hand_controller"はnode名
  ros::init(argc, argv, "hand_controller");
  ///-- ノードハンドラ
  ros::NodeHandle n;

  ///-- 各Subscriber、<>はmsg名、()内は順にtopic名、キューサイズ、コールバック関数
  ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("joy", 10, &joyCallback);
  ros::Subscriber dynamixel1_sub = n.subscribe<dynamixel_msgs::JointState>("tilt1_controller/state", 10, &dynamixel1Callback);
  ros::Subscriber dynamixel2_sub = n.subscribe<dynamixel_msgs::JointState>("tilt2_controller/state", 10, &dynamixel2Callback);
  ros::Subscriber dynamixel3_sub = n.subscribe<dynamixel_msgs::JointState>("tilt3_controller/state", 10, &dynamixel3Callback);
  ros::Subscriber dynamixel4_sub = n.subscribe<dynamixel_msgs::JointState>("tilt4_controller/state", 10, &dynamixel4Callback);
  ros::Subscriber dynamixel5_sub = n.subscribe<dynamixel_msgs::JointState>("tilt5_controller/state", 10, &dynamixel5Callback);

  ///-- 各Publisher、<>はmsg名、()内は順にtopic名、キューサイズ
  ros::Publisher tilt1_pub = n.advertise<std_msgs::Float64>("tilt1_controller/command", 1);
  ros::Publisher tilt2_pub = n.advertise<std_msgs::Float64>("tilt2_controller/command", 1);
  ros::Publisher tilt3_pub = n.advertise<std_msgs::Float64>("tilt3_controller/command", 1);
  ros::Publisher tilt4_pub = n.advertise<std_msgs::Float64>("tilt4_controller/command", 1);
  ros::Publisher tilt5_pub = n.advertise<std_msgs::Float64>("tilt5_controller/command", 1);

  ///-- publishするための変数の定義
  ///-- std_msgs::Float64はメンバ変数として"data"を持っている
  ///-- cppのクラスという概念の要素なので詳しくはcppを勉強しよう
  std_msgs::Float64 command1, command2, command3, command4, command5;
  ///-- 各変数定義
  int count = 0;
  float tau[5] = {0.0};
  float theta_dot[5] = {0.0};
  ///-- 制御周期
  const float delta_t = 0.02;
  ///-- 制御周期の決定(50Hz=0.02sec)
  ros::Rate loop_rate(50);

  ///-- 制御してる部分(ctrl+Cが押されない限り回り続ける)
  while(ros::ok()){
    ///-- ヤコビ行列の転置をかける関数
    JacobCross(theta, input, tau);

    ///-- 各dynamixelの角速度に変換
    for(int i=0; i<5; i++){
      theta_dot[i] = K_v_inv[i] * tau[i];
    }

    ///-- publish変数に目標角度を代入
    command1.data =   theta[0] + theta_dot[0] * delta_t;
    command2.data = -(theta[1] + theta_dot[1] * delta_t);
    command3.data =   theta[2] + theta_dot[2] * delta_t;
    command4.data =   theta[3] + theta_dot[3] * delta_t;
    command5.data =   theta[4] + theta_dot[4] * delta_t;

    ///-- CRANE+の初期化(2秒間)
    if(count < 100){
      command1.data = 0.6;
      command2.data = 1.2;
      command3.data = 0.6;
      command4.data = 0.0;
      command5.data = 0.0;

      count++;
    }

    ///-- publish
    tilt1_pub.publish(command1);
    tilt2_pub.publish(command2);
    tilt3_pub.publish(command3);
    tilt4_pub.publish(command4);
    tilt5_pub.publish(command5);

    ///-- 制御を最速で回すためのコマンド
    ros::spinOnce();
    ///-- 決められた制御周期より早い場合はそれまで待機するコマンド
    loop_rate.sleep();
  }
}
