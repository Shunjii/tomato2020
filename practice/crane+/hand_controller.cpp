#include <iostream>
#include <fstream>
#include <math.h>
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <dynamixel_msgs/JointState.h>
#include <sensor_msgs/Joy.h>

using namespace std;

///-- 以下、随時__________として空欄を用意している
///-- Wikiで得られた情報を基に例に従って埋めるように
///-- !!!空欄を埋める際は_(アンダーバー)を消すように注意!!!

///-- リンクの長さ
float Link[4] = {0.078, 0.083, 0.066, 0.066};
///-- dynamixelの角度変位に対するゲイン(後々調整すること、大きな値を入れるとCRANE+が暴走するので注意)
float K_v_inv[5] = {1.0, 1.0, 1.0, 1.0, 1.0};
///-- グローバル変数定義
float input[5] = {0.0};
float theta[5] = {0.0};

///-- ジョイスティックの情報をsubscribeする関数
///-- 関数の引数の空欄も埋めること
///-- Hint) msg名が入る
void joyCallback(const __________::ConstPtr& joy)
{
  ///-- 各自由度に対するゲイン
  ///-- 例に従って自分で決めたジョイスティックの割り当てを空欄を埋めること
  ///-- 例) input[5] = 1.0 * joy->axes[1];
  input[0] = __________; ///- X
  input[1] = __________; ///- Y
  input[2] = __________; ///- Yaw
  input[3] = __________; ///- Pitch
  input[4] = __________; ///- End-Effector
}

///-- 以下5つ、dynamixelの現在角度をsubscribeする関数
///-- 例に従って現在角度を取得するように空欄を埋めること
///-- 例) theta[5] = dynamixel6->???(Wikiからわかるはず)
void dynamixel1Callback(const __________::ConstPtr& dynamixel1)
{
  theta[0] = __________;
}

void dynamixel2Callback(const __________::ConstPtr& dynamixel2)
{
  ///-- !!!マイナスは消さないように(2軸はdynamixelの向きが逆転しているためマイナスがつく)!!!
  theta[1] = -__________;
}

void dynamixel3Callback(const __________::ConstPtr& dynamixel3)
{
  theta[2] = __________;
}

void dynamixel4Callback(const __________::ConstPtr& dynamixel4)
{
  theta[3] = __________;
}

void dynamixel5Callback(const __________::ConstPtr& dynamixel5)
{
  theta[4] = __________;
}

///-- ヤコビ行列の転置を左からかける関数
///-- 各空欄を埋めること
///-- !!!ここでのヤコビ行列は転置したものを記述するように!!!
///-- Hint) Jacob_vT[0][1]には元のヤコビ行列の2行1列成分が入る
void JacobCross(float theta[], float input[], float tau[])
{
  float Jacob_vT[3][3];

  Jacob_vT[0][0] = __________;
  Jacob_vT[0][1] = __________;
  Jacob_vT[0][2] = __________;
  Jacob_vT[1][0] = __________;
  Jacob_vT[1][1] = __________;
  Jacob_vT[1][2] = __________;
  Jacob_vT[2][0] = __________;
  Jacob_vT[2][1] = __________;
  Jacob_vT[2][2] = __________;

  ///-- 入力値を各dynamixelの目標トルクに変換(詳しくはロボティクスで)
  for(int i=0; i<3; i++){
    tau[i] = Jacob_vT[i][0] * input[0] + Jacob_vT[i][1] * input[1] + Jacob_vT[i][2] * input[2];
  }

  tau[3] = input[3];
  tau[4] = input[4];
}

int main(int argc, char** argv)
{
  ///-- nodeの初期化、"hand_controller"はnode名
  ros::init(argc, argv, "hand_controller");
  ///-- ノードハンドラ
  ros::NodeHandle n;

  ///-- 各Subscriber、<>はmsg名、()内は順に(頭の/を抜いた)topic名、キューサイズ、コールバック関数
  ///-- 空欄を埋めること(dynamixelについては数字も合わせるように)
  ros::Subscriber joy_sub = n.subscribe<__________>("__________", 10, &joyCallback);
  ros::Subscriber dynamixel1_sub = n.subscribe<__________>("__________", 10, &dynamixel1Callback);
  ros::Subscriber dynamixel2_sub = n.subscribe<__________>("__________", 10, &dynamixel2Callback);
  ros::Subscriber dynamixel3_sub = n.subscribe<__________>("__________", 10, &dynamixel3Callback);
  ros::Subscriber dynamixel4_sub = n.subscribe<__________>("__________", 10, &dynamixel4Callback);
  ros::Subscriber dynamixel5_sub = n.subscribe<__________>("__________", 10, &dynamixel5Callback);

  ///-- 各Publisher、<>はmsg名、()内は順に(頭の/を抜いた)topic名、キューサイズ
  ///-- 空欄を埋めること
  ros::Publisher tilt1_pub = n.advertise<__________>("__________", 1);
  ros::Publisher tilt2_pub = n.advertise<__________>("__________", 1);
  ros::Publisher tilt3_pub = n.advertise<__________>("__________", 1);
  ros::Publisher tilt4_pub = n.advertise<__________>("__________", 1);
  ros::Publisher tilt5_pub = n.advertise<__________>("__________", 1);

  ///-- publishするための変数の定義
  ///-- std_msgs::Float64はメンバ変数として"data"を持っている
  ///-- cppのクラスという概念の要素なので詳しくはcppを勉強しよう
  std_msgs::Float64 command1, command2, command3, command4, command5;
  ///-- 各変数定義
  int count = 0;
  float tau[5] = {0.0};
  float theta_dot[5] = {0.0};
  ///-- 制御周期
  const float delta_t = 0.02;
  ///-- 制御周期の決定(50Hz=0.02sec)
  ros::Rate loop_rate(50);

  ///-- 制御してる部分(ctrl+Cが押されない限り回り続ける)
  while(ros::ok()){
    ///-- ヤコビ行列の転置をかける関数
    JacobCross(theta, input, tau);

    ///-- 各dynamixelの目標角速度に変換
    ///-- CRANE+を接続し動作することが確認できた後はゲインの調整をすること
    for(int i=0; i<5; i++){
      theta_dot[i] = K_v_inv[i] * tau[i];
    }

    ///-- publish変数に目標角度を代入
    ///-- (現在角度)+(目標角速度)*(制御周期)=(目標角度)となるように空欄を埋めること
    ///-- !!!2軸のマイナスは消さないように!!!
    command1.data =   __________;
    command2.data = -(__________);
    command3.data =   __________;
    command4.data =   __________;
    command5.data =   __________;

    ///-- CRANE+の初期化(2秒間)
    if(count < 100){
      command1.data = 0.6;
      command2.data = 1.2;
      command3.data = 0.6;
      command4.data = 0.0;
      command5.data = 0.0;

      count++;
    }

    ///-- publish
    tilt1_pub.publish(command1);
    tilt2_pub.publish(command2);
    tilt3_pub.publish(command3);
    tilt4_pub.publish(command4);
    tilt5_pub.publish(command5);

    ///-- 制御を最速で回すためのコマンド
    ros::spinOnce();
    ///-- 決められた制御周期より早い場合はそれまで待機するコマンド
    loop_rate.sleep();
  }
}
