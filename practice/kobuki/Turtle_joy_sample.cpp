// ===========================================
// Turtleをjoystickで操作するためのサンプルプログラム
// ===========================================

/* ヘッダファイルの読み込み */
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>


/* クラス定義 */
class TurtleOperation
{
public:
    // 関数はこっち
    TurtleOperation(); // コンストラクタ(実行すると最初に呼ばれ、変数を初期化する役割)
    void joyCallback(const sensor_msgs::Joy::ConstPtr &joy); // joystickの信号を受け取る

private:
    //変数はこっち
    ros::NodeHandle nh; // これは絶対いいる。　ノードを管理するための変数
    ros::Subscriber joy_sub; // subscriberの変数宣言
    ros::Publisher  turtle_pub; // publisherの変数宣言
    double          scaleLinear, scaleAngular; // 使いたい変数宣言(C言語と一緒の形式)
};

/* クラス内の関数 */
TurtleOperation::TurtleOperation()
{

    // subscriber
    joy_sub = nh.subscribe <sensor_msgs::Joy>("joy", 1, &TurtleOperation::joyCallback, this);

    // publisher
    turtle_pub = nh.advertise <geometry_msgs::Twist>("/turtle1/cmd_vel", 1); //Kobuki実機

    // 変数の初期化
    scaleLinear  = 0.03;
    scaleAngular = 0.3;

}


/* ジョイスティックの信号を受け取ったら呼ばれる関数 */
void TurtleOperation::joyCallback(const sensor_msgs::Joy::ConstPtr &joy)
{
    //変数の宣言(int a; とかと一緒)
    //topicの型に合わせた変数を定義
    geometry_msgs::Twist twist;

    // joyの信号を使いたいときはこんな感じ
    twist.linear.x  = scaleLinear * (joy->buttons[0] -joy->buttons[2]);
    twist.angular.z = scaleAngular *(joy->buttons[1] -joy->buttons[3]);

    turtle_pub.publish(twist);
}

/* main関数(そのままでよい) */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "turtle_operation");
    TurtleOperation turtle_operation;
    ros::spin();
}
